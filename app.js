const express = require('express');
const sequelize = require('./database');

const cors = require('cors')
const app = express()
const PORT =  8080

require('./model')

app.use(cors())
app.use(express.json())

app.use('/', require('./routes'));


app.listen(PORT, () => {
    console.log(`Server is started at ${PORT}`)
})
