const router = require('express').Router();

const Controller = require('./controller');


router.post('/add', Controller.add);
router.post('/get',  Controller.get);
router.post('/get/:id',  Controller.getUserById);
router.post('/update/:id',  Controller.updateById);
router.post('/delete',  Controller.deleteAll);

module.exports = router;