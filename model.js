const { Sequelize, DataTypes } = require('sequelize');


const sequelize = require('./database');


const User = sequelize.define('User', {
    id :{
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  mobile: {
    type: DataTypes.STRING,
    allowNull: false
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false
}, 
created_at: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: Sequelize.Sequelize.literal("CURRENT_TIMESTAMP"),
},
updated_at: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: Sequelize.Sequelize.literal("CURRENT_TIMESTAMP"),
},

}
);


module.exports = User
