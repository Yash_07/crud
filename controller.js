const sequelize = require('./database');
const User = require('./model');


exports.add = async (req,res) => {
  
        const _b = req.body;
        let user = {
            email: _b.email,
            name: _b.name,
            mobile: _b.mobile
        }

      await  User.create(user).then(u => {
            res.status(200).send({ ...u.toJSON(), status: true });
        })
        .catch(err => {
            console.error(err);
            res.status(400).json({ error: err.message, status: false });
        });
  
        console.log("yah")

}

exports.get = async(req,res) => {

await User.findAll().then(us => {

    if(!us){
        return res.send({"msg":"No users found"})
    }

    res.status(200).send(us)

}).catch(err => {
    console.error(err);
    res.status(400).json({ error: err.message, status: false });
})

}

exports.getUserById = async (req,res) => {
    const i = req.params.id
    await User.findAll({where : {id : i}}).then(us => {
        
        if(!us){
            return res.send({"msg":"No user found"})
        }

        res.status(200).send(us);
        
    }).catch(err => {
        console.error(err);
        res.status(400).json({ error: err.message, status: false });
    })
}


exports.deleteAll = async (req,res) => {
   await User.destroy({where :{}}).then(re => {
       res.send({"msg" : "All deleted"})
   }).catch(err => {
    console.error(err);
    res.status(400).json({ error: err.message, status: false });
})
}

exports.deleteById = async (req,res) =>{
    const i = req.params.id;
    await User.destroy({where : {id : i}}).then(de => {
        res.send({"msg" : "All deleted"})
    }).catch(err => {
     console.error(err);
     res.status(400).json({ error: err.message, status: false });
 })
    
}


exports.updateById = async (req,res) => {
    const i = req.params.id;
    
    const {name , email , mobile} = req.body
   await User.update(
        {
            name,
            email,
            mobile  
        },
        { 
            where: 
            {
                id: i
            }
        }
    ).then(c => {
          return res.send({c})
    });



}



